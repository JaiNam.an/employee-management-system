from . import views


class Views:
    role_view = views.RoleViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    role_retrieve = views.RoleViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy'
    })

    client_view = views.ClientViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    client_retrieve = views.ClientViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy'
    })

    project_view = views.ProjectViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    project_retrieve = views.ProjectViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy',
    })

    employee_view = views.EmployeeViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    employee_retrieve = views.EmployeeViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy',
    })

    department_view = views.DepartmentViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    department_retrieve = views.DepartmentViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy',
    })

    edetail_view = views.EDetailViewSet.as_view({
        'get': 'list',
        'post': 'create',
    })

    edetail_retrieve = views.EDetailViewSet.as_view({
        'put': 'update',
        'get': 'retrieve',
        'delete': 'destroy',
    })
