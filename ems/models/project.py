from .client import Client
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Project(models.Model):
    project_id = models.CharField(primary_key=True, max_length=5, unique=True)
    project_name = models.CharField(max_length=50, null=False)
    project_rating = models.IntegerField(null=False,
                                         help_text='Rate between 1-5',
                                         validators=[MinValueValidator(0), MaxValueValidator(5)])
    client = models.OneToOneField(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.project_id} - {self.project_name}'
