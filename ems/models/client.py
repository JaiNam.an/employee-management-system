from django.db import models
from django.core.validators import MinLengthValidator, MaxLengthValidator


class Client(models.Model):
    client_id = models.CharField(primary_key=True, max_length=5)
    client_name = models.CharField(max_length=25, null=False)
    contact_no = models.CharField(max_length=10, null=False, unique=True,
                                  validators=[MinLengthValidator(10), MaxLengthValidator(10)])

    def __str__(self):
        return f'{self.client_id} - {self.client_name}'
