from django.contrib import admin
from .models import Client, Role, Project, Employee, Department, EmployeeDetail


# Register your models here.


class RoleAdmin(admin.ModelAdmin):
    list_display = ('role_id', 'role_name')


class ClientAdmin(admin.ModelAdmin):
    list_display = ('client_id', 'client_name', 'contact_no')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('project_id', 'project_name', 'project_rating', 'client')


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('employee_id', 'employee_name', 'contact_no', 'salary', 'joining_date')


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('department_id', 'department_name')


class EmployeeDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'employee', 'manager')


admin.site.register(Role, RoleAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(EmployeeDetail, EmployeeDetailAdmin)
