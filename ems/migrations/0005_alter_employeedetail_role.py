# Generated by Django 4.1.3 on 2022-11-07 17:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ems', '0004_alter_employeedetail_employee'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeedetail',
            name='role',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ems.role'),
        ),
    ]
